# Redis Noob thoughts

## Questions to answer:

1. What is Redis?

    Redis is in-memory database, "in-memory" mean that data is stored in RAM, with option to frequent write to disk.
    Since it is in-memory, data look up + writes are super fast, RAM lookup is 100ns where as Disk lookup is 2ms, so it is around 10000 times faster than any Database which saves data in disk (for eg: Postgres). 
    
    Source : [Number every programmer should know](https://colin-scott.github.io/personal_website/research/interactive_latency.html)

2. What's something, I should look in redis while comparing with other Databases?

    Redis is single threaded, so there is no chance of issues happening because of Race condition.
    Also because of this, single threaded behaviour, Redis give Consistency & Isolation of "ACID" behaviour as guarantee.
    Moreover, It is NoSQL database, so we have fliexibility of data structure stored.

 3. How is data saved, can I read it without redis server being up?
 4. what is cluster?
 5. How is data division works?
 6. Who decides which slot for Data to go? is it on application or goes to redis cluster?
 7. What if we send data to different slot?
 8. Can I use single IP and still use cluster?
 9. How does the rebalancing happens if I add more nodes in redis, do I need to update application, to make it update the slot where to send key?
 10. Reliability 
     1. Master slave architecture
     2. cluster
 11. What happens in case of failover, how is consenses reached?